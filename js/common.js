$(document).ready(function()
{
    var userAgent = window.navigator.userAgent.toLowerCase();
    if(userAgent.match(/(msie|MSIE)/) || userAgent.match(/(T|t)rident/))
    {
        $('body').html("<p id=\"no-support\">このページはInternet Exploerをサポートしていません、最新版のChrome又はFirefoxを使用してください</p>");
    }
    if(userAgent.indexOf('edge') != -1)
    {
        $('.header-top-center ul').prepend("<p id=\"no-support\">このページはMisrosoft Edgeはサポートしていません、最新版のChrome又はFirefoxを使用してください</p>");
    }

    $('header').css('top','-200px');
    $('header').css('opacity','0');
    $('#header-wave').css('opacity','0.5');
    $('.contant').css('opacity','0');
    $('.contant').css('margin-top','0px');

    $('.card-contant').mouseover(function(e){
        $("#header-subpage-title h2").text($('h3',this).text());
        $("#header-subpage-description p").html($('p',this).html());
        $(".header-subpage-infomation").stop(false,true).animate({ opacity : 1},500,'easeOutCirc');
    }).mouseout(function(e){
        $(".header-subpage-infomation").stop(false,true).animate({ opacity : 0},500,'easeOutCirc');
    });
});

$(window).on('load', function()
{
    setWaveCanvas();
    showAnimations();
});

function showAnimations()
{
    $('header').animate({ opacity: 1 },
    {
        duration: 1000,
        easing: 'easeOutCirc'
    }).animate({ top: 0 }, 800, 'easeInOutQuart',function()
    {
        $('.contant').animate({ 'margin-top': '250px' },
        {
            duration: 1000,
            easing: 'easeOutCirc',
            queue: false
        }).animate({ opacity: 1 },
        {
            duration: 1000,
            easing: 'easeOutCirc'
        });
    });
}

function setWaveCanvas()
{
    let pageArray = [ "/pages/dev/", "/pages/document/", "/pages/music/", "/contact.html"];
    let pageColorArray = [ "#81d4fa", "#b39ddb", "#9e9e9e", "#ef9a9a"];
    let nowWaveColor = "#fff";

    for(var i = 0; i < pageArray.length; i++)
        if(location.pathname.startsWith(pageArray[i])) nowWaveColor = pageColorArray[i];
    
    var width = $(window).width();
    window.siren = new Siren({
        target: 'header-wave',
        height: 200,
        width: width,
        color: nowWaveColor,
        waves: [
            {
                alpha: 0.1,
                yOffset: 40,
                speed: 0.02 * 0.4,
                angleStep: 0.0075,
                peak: 35,
                isPositive: true
            },
            {
                alpha: 0.2,
                yOffset: 0,
                speed: 0.025 * 0.4,
                angleStep: 0.0055,
                peak: 30,
                isPositive: true
            }
        ]
    });
    window.siren.draw();
}

window.onresize = function () {
    var width = $(window).width();
    window.siren.update({
        width: width
    });
    window.siren.draw();
}